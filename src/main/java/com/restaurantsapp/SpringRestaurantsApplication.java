package com.restaurantsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRestaurantsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRestaurantsApplication.class, args);
	}

}
