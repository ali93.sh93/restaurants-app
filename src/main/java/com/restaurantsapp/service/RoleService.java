package com.restaurantsapp.service;

import com.restaurantsapp.model.Role;

public interface RoleService {
    Role findByName(String name);
}
