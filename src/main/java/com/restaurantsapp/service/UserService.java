package com.restaurantsapp.service;

import com.restaurantsapp.model.User;
import com.restaurantsapp.model.UserDto;

import java.util.List;

public interface UserService {
    User save(UserDto user);
    List<User> findAll();
    User findOne(String username);
}
