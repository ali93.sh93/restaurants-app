package com.restaurantsapp.service.impl;

import com.restaurantsapp.dao.RoleDao;
import com.restaurantsapp.model.Role;
import com.restaurantsapp.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "roleService")
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    @Override
    public Role findByName(String name) {
        Role role = roleDao.findRoleByName(name);
        return role;
    }
}
